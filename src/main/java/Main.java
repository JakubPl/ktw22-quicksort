import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] numbers = {71, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        quickSort(numbers);
    }

    private static void quickSort(int numbers[]) {
        quickSort(0, numbers.length - 1, numbers);
    }

    private static void quickSort(int start, int end, int[] numbers) {
        if (end > start) {
            int partitionIndex = partitionArray(start, end, numbers);
            System.out.println(Arrays.toString(numbers));
            quickSort(start, partitionIndex - 1, numbers);
            quickSort(partitionIndex + 1, end, numbers);
        }
    }

    private static int partitionArray(int start, int end, int[] numbers) {
        int pivotIndex = pickPivot(start, end);
        int pivotValue = numbers[pivotIndex];
        swap(pivotIndex, start, numbers);
        int lastSwappedIndex = start;
        for (int i = start + 1; i <= end; i++) {
            int currentValue = numbers[i];
            if (currentValue < pivotValue) {
                lastSwappedIndex++;
                swap(i, lastSwappedIndex, numbers);
            }
        }
        //TODO: bug pickPivot
        swap(start, lastSwappedIndex, numbers);
        return lastSwappedIndex;
    }

    private static void swap(int a, int b, int[] numbers) {
        int tmp = numbers[a];
        numbers[a] = numbers[b];
        numbers[b] = tmp;
    }

    private static int pickPivot(int start, int end) {
        return end;
    }
}
